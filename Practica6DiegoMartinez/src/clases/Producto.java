package clases;

public class Producto {
	String codigo;
	double precio;
	int numProductos;
	Cliente cliente;

	public Producto() {
		this.codigo = "";
		this.precio = 0;
		this.numProductos = 0;

	}

	public Producto(String codigo, double precio, int numProductos) {
		this.codigo = codigo;
		this.precio = precio;
		this.numProductos = numProductos;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public int getNumProductos() {
		return numProductos;
	}

	public void setNumProductos(int numProductos) {
		this.numProductos = numProductos;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	@Override
	public String toString() {
		return "Producto [codigo=" + codigo + ", precio=" + precio + ", numProductos=" + numProductos + ", cliente="
				+ cliente + "]";
	}

	
	

}

