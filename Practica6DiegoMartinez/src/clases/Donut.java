package clases;

public class Donut extends Producto {
	int pack;
	String sabor;
	int cantidadpack;
	public Donut() {
		super();
		this.pack = 0;
		this.sabor = "";
		this.cantidadpack = 0;
	}
	public Donut(String codigo, double precio, int numProductos, int pack, String sabor, int cantidadpack) {
		this.pack = pack;
		this.sabor = sabor;
		this.cantidadpack = cantidadpack;
	}
	public int getPack() {
		return pack;
	}
	public void setPack(int pack) {
		this.pack = pack;
	}
	public String getSabor() {
		return sabor;
	}
	public void setSabor(String sabor) {
		this.sabor = sabor;
	}
	public int getCantidadpack() {
		return cantidadpack;
	}
	public void setCantidadpack(int cantidadpack) {
		this.cantidadpack = cantidadpack;
	}
	@Override
	public String toString() {
		return "Donut [pack=" + pack + ", sabor=" + sabor + ", cantidadpack=" + cantidadpack + ", codigo=" + codigo
				+ ", precio=" + precio + ", numProductos=" + numProductos + "]";
	}
	
	
	
}
