package clases;

public class Pastel extends Producto {
	int pisos;
	String sabor;
	String tipo;
	
	public Pastel() {
		super();
		this.pisos = 0;
		this.sabor = "";
		this.tipo = "";
	}

	public Pastel(String codigo, double precio, int numProductos, int pisos, String sabor, String tipo) {
		super(codigo, precio, numProductos);
		this.pisos = pisos;
		this.sabor = sabor;
		this.tipo = tipo;
	}

	public int getPisos() {
		return pisos;
	}

	public void setPisos(int pisos) {
		this.pisos = pisos;
	}

	public String getSabor() {
		return sabor;
	}

	public void setSabor(String sabor) {
		this.sabor = sabor;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	@Override
	public String toString() {
		return "Pastel [pisos=" + pisos + ", sabor=" + sabor + ", tipo=" + tipo + ", codigo=" + codigo + ", precio="
				+ precio + ", numProductos=" + numProductos + "]";
	}
	
	
}
