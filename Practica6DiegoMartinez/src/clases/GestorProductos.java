package clases;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;


public class GestorProductos {

	static Scanner input = new Scanner(System.in);
	private ArrayList<Cliente> listaCliente;
	private ArrayList<Donut> listaDonut;
	private ArrayList<Pastel> listaPastel;

	public GestorProductos() {
		listaCliente = new ArrayList<Cliente>();
		listaDonut = new ArrayList<Donut>();
		listaPastel = new ArrayList<Pastel>();
	}

	public void altaCliente() {
		System.out.println(" dime el nombre del cliente al que quieres dar de alta:");
		String nombre = input.nextLine();
		if (!compruebaCliente(nombre)) {
			Cliente nuevoCliente = new Cliente();
			nuevoCliente.setNombre(nombre);
			System.out.println("Introduce el apellido");
			String apellido = input.nextLine();
			nuevoCliente.setApellido(apellido);
			System.out.println("Introduce la edad:");
			int edad = input.nextInt();
			nuevoCliente.setEdad(edad);
			input.nextLine();
			listaCliente.add(nuevoCliente);
		}	
	}
	public boolean compruebaCliente(String nombre) {
		for (Cliente cliente : listaCliente) {
			if (cliente != null && cliente.getNombre().equals(nombre)) {
				return true;
			}
		}
		return false;

	}
	public void listarCliente() {
		 
		 
		 for (int i =0; i<listaCliente.size(); i++) {
			 Cliente cliente = listaCliente.get(i);
			 if (cliente != null) {
				 System.out.println(cliente);
			 }
		 } 
	 }

	public Cliente buscarCliente() {
		System.out.println("Introduce el nombre del cliente");
		String nombreCliente = input.nextLine().toLowerCase();
		for (Cliente cliente : listaCliente) {
			if (cliente != null && cliente.getNombre().equals(nombreCliente)) {
				return cliente;
			}
		}
		System.out.println("no tenemos ningun cliente con ese nombre");
		return null;
	}


	public void eliminarCliente() {
		System.out.println("Introduce en nombre del cliente :");
		String nombreCliente = input.nextLine().toLowerCase();
		if (compruebaCliente(nombreCliente)) {
			Iterator<Cliente> iteradorCliente = listaCliente.iterator();
			while (iteradorCliente.hasNext()) {
				Cliente cliente = iteradorCliente.next();
				if (cliente.getNombre().equals(nombreCliente)) {
					iteradorCliente.remove();
				}
			}
			System.out.println("el cliente fue eliminado.");
		} else {
			System.out.println("no tenemos ningun cliente con ese nombre");
		}
	}

	
	public void altaDonut() {
		System.out.println("Introduce el codigo:");
		String codigo = input.nextLine();
		if (!compruebadonut(codigo)) {
			Donut nuevoDonut = new Donut();
			nuevoDonut.setCodigo(codigo);;
					System.out.println("Introduce el precio:");
					double precio = input.nextDouble();
					nuevoDonut.setPrecio(precio);
					input.nextLine();
					System.out.println("Introduce el numero de Productos:");
					int numProductos = input.nextInt();
					nuevoDonut.setNumProductos(numProductos);
					System.out.println("Introduce el numero  de packs:");
					int pack = input.nextInt();
					nuevoDonut.setPack(pack);;
					input.nextLine();
			System.out.println("Introduce el sabor");
			String sabor = input.nextLine();
			nuevoDonut.setSabor(sabor);
			System.out.println("Introduce numero de donuts que hay en cada pack:");
			int cantidadpack = input.nextInt();
			nuevoDonut.setCantidadpack(cantidadpack);
			input.nextLine();
			listaDonut.add(nuevoDonut);
		} else {
			System.out.println("El donut ya existe");
		}
	}
	
	
	
	public boolean compruebadonut(String codigo) {
		for (Donut donut : listaDonut) {
			if (donut != null && donut.getCodigo().equals(codigo)) {
				return true;
			}
		}
		return false;
	}
	
	
	
	public void listarDonuts() {
		for (Producto pro : listaDonut) {
			if (pro != null) {
				System.out.println(pro);
			}
		}
	}
	
	public Producto buscarDonuts() {
		System.out.println("Introduce el codigo del donut:");
		String codigoDonut = input.nextLine().toLowerCase();
		for (Donut producto : listaDonut) {
			if (producto != null && producto.getCodigo().equals(codigoDonut)) {
				return producto;
			}
		}
		System.out.println("No existe ningun donut por ese codigo.");
		return null;
	}
	
	
	public void eliminarDonut() {
		System.out.println("Introduce el codigo donut que quieras eliminar:");
		String codigo = input.nextLine().toLowerCase();
		if (compruebadonut(codigo)) {
			Iterator<Donut> iteradorCd = listaDonut.iterator();
			while (iteradorCd.hasNext()) {
				Donut producto = iteradorCd.next();
				if (producto.getCodigo().equals(codigo)) {
					iteradorCd.remove();
				}
			}
			System.out.println("donut eliminado.");
		} else {
			System.out.println("codigo de donut inexistente.");
		}
	}
	
	public void asignarDonut() {
		Cliente clien = buscarCliente();
		if (clien != null) {
			Producto pro = buscarDonuts();
			if (pro != null) {
				pro.setCliente(clien);
				System.out.println("");
				System.out.println("Cliente asignado");
			}
		}
	}
	
	
	public void altaPastel() {
		System.out.println("Introduce el codigo:");
		String codigo = input.nextLine();
		if (!compruebaPastel(codigo)) {
			Pastel nuevoPastel = new Pastel();
			nuevoPastel.setCodigo(codigo);;
					System.out.println("Introduce el precio:");
					double precio = input.nextDouble();
					nuevoPastel.setPrecio(precio);
					input.nextLine();
					System.out.println("Introduce el numero de Productos:");
					int numProductos = input.nextInt();
					nuevoPastel.setNumProductos(numProductos);
					input.nextLine();
					System.out.println("Introduce numero de pisos:");
					int pisos = input.nextInt();
					nuevoPastel.setPisos(pisos);
					input.nextLine();
			System.out.println("Introduce el sabor");
			String sabor = input.nextLine();
			nuevoPastel.setSabor(sabor);
			System.out.println("Introduce el tipo de pastel *cumple, boda, comunion*:");
			String tipo = input.nextLine();
			nuevoPastel.setTipo(tipo);
			listaPastel.add(nuevoPastel);
		} else {
			System.out.println("El pastel ya existe");
		}
	}
	

	public boolean compruebaPastel(String codigo) {
		for (Pastel pastel : listaPastel) {
			if (pastel != null && pastel.getCodigo().equals(codigo)) {
				return true;
			}
		}
		return false;
	}
	

	
	public void listarPasteles() {
		for (Producto pro : listaPastel) {
			if (pro != null) {
				System.out.println(pro);
			}
		}
	}

	public Producto buscarPastel() {
		System.out.println("Introduce el codigo del Pastel:");
		String codPastel = input.nextLine().toLowerCase();
		for (Pastel producto : listaPastel) {
			if (producto != null && producto.getCodigo().equals(codPastel)) {
				return producto;
			}
		}
		System.out.println("No existe ningun pastel con ese codigo.");
		return null;
	}

	public void eliminarPastel() {
		System.out.println("Introduce el codigo del pastel que quieras eliminar:");
		String codigo = input.nextLine().toLowerCase();
		if (compruebaPastel(codigo)) {
			Iterator<Pastel> iteradorCd = listaPastel.iterator();
			while (iteradorCd.hasNext()) {
				Pastel producto = iteradorCd.next();
				if (producto.getCodigo().equals(codigo)) {
					iteradorCd.remove();
				}
			}
			System.out.println("pastel eliminado.");
		} else {
			System.out.println("codigo de pastel inexistente.");
		}
	}


public void asignarPastel() {
	Cliente clien = buscarCliente();
	if (clien != null) {
		Producto pro = buscarPastel();
		if (pro != null) {
			pro.setCliente(clien);
			System.out.println("");
			System.out.println("Cliente asignado");
		}
	}
}

}
