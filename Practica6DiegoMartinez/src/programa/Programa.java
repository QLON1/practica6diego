package programa;

import java.util.Scanner;

import clases.GestorProductos;

public class Programa {

	static Scanner input = new Scanner(System.in);
	
	public static void main(String[] args) {
		
		GestorProductos tps = new GestorProductos();
		int opcion;
		do {
			System.out.println("1.	Dar de alta un cliente");
			System.out.println("2.	Listar clientes");
			System.out.println("3.	Buscar clientes");
			System.out.println("4.	Eliminar clientes");
			System.out.println();
			System.out.println();
			System.out.println();
			System.out.println("5.	Dar de alta un donut");
			System.out.println("6.	Listar donuts");
			System.out.println("7.	Buscar donuts");
			System.out.println("8.	Eliminar donuts");
			System.out.println("9.	Asignar un cliente");
			System.out.println();
			System.out.println();
			System.out.println();
			System.out.println("10.	Dar de alta Pastel");
			System.out.println("11.	Listar Pastel");
			System.out.println("12.	Buscar Pastel");
			System.out.println("13.	Eliminar Pastel");
			System.out.println("14. Asignar un cliente");
			System.out.println();
			System.out.println("15. - Salir");
			opcion = input.nextInt();

			switch (opcion) {
			case 1:
				tps.altaCliente();
				break;
			case 2:
				tps.listarCliente();
				break;
			case 3:
				System.out.println(tps.buscarCliente());
				break;
			case 4:
				tps.eliminarCliente();
				break;
			case 5:
				tps.altaDonut();
				break;
			case 6:
				tps.listarDonuts();
				break;
			case 7:
				System.out.println(tps.buscarDonuts());
				break;
			case 8:
				tps.eliminarDonut();
				break;
			case 9:
				tps.asignarDonut();
				break;
			case 10:
				tps.altaPastel();
				break;
			case 11:
				tps.listarPasteles();
				break;
			case 12:
				System.out.println(tps.buscarPastel());
				break;
			case 13:
				tps.eliminarPastel();
				break;
			case 14:
				tps.asignarPastel();
				break;
			case 15:
				System.out.println("Fin del programa.");
				System.exit(0);
				break;
			default:
				System.out.println("la opcion selecionada no es correcta , prueba otra vez.");
			}

		} while (opcion != 15);

		input.close();
	}

}